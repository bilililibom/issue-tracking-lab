# i n c l u d e   < i o s t r e a m >    
 # i n c l u d e   < c t i m e >    
 u s i n g   n a m e s p a c e   s t d ;  
 v o i d   F i l l A r r a y ( i n t * ,   i n t ) ;  
 v o i d   P r i n t A r r a y ( i n t * ,   i n t ) ;  
 v o i d   F o r m a t T i m e F r o m S e c o n d s T o M i n u t e s ( i n t ,   i n t   & ,   i n t   & ) ;  
 v o i d   S h o w F o r m a t t e d T i m e ( i n t ,   i n t ,   i n t ) ;  
 i n t   P r o m t F o r I n p u n t ( ) ;  
 v o i d   G e t C o u n t ( i n t ,   i n t * ,   i n t ,   i n t & ) ;  
 i n t   m a i n ( )  
 {  
 	 s r a n d ( t i m e ( 0 ) ) ;  
 	 i n t   s i z e   =   r a n d ( )   %   2 5   +   2 5 ;  
 	 c o u t   < <   " S i z e   =   "   < <   s i z e   < <   e n d l ;  
 	 i n t   * m a s s   =   n e w   i n t [ s i z e ] ;  
 	 F i l l A r r a y ( m a s s ,   s i z e ) ;  
 	 P r i n t A r r a y ( m a s s ,   s i z e ) ;  
 	  
 	 f o r   ( i n t   i   =   0 ;   i   <   s i z e ;   i + + )  
 	 {  
 	 	 i n t   m i n u t e s   =   0 ,   s e c o n d s   =   0 ;  
 	 	 i n t   T i m e   =   m a s s [ i ] ;  
 	 	 F o r m a t T i m e F r o m S e c o n d s T o M i n u t e s ( T i m e ,   m i n u t e s ,   s e c o n d s ) ;  
 	 	 S h o w F o r m a t t e d T i m e ( i ,   m i n u t e s ,   s e c o n d s ) ;  
 	 }  
 	  
 	 i n t   S e c o n d s   =   S e c o n d s   =   P r o m t F o r I n p u n t ( ) ;  
 	 i n t   c o u n t   =   0 ;  
 	 G e t C o u n t ( S e c o n d s ,   m a s s ,   s i z e ,   c o u n t ) ;  
 	 c o u t   < <   e n d l   < <   " T h e   n u m b e r   o f   c o m p e t i t i o n   r e s u l t s   e x c e e d i n g   t h e   e n t e r e d   v a l u e   i s   "   < <   c o u n t   < <   e n d l ;  
 	 r e t u r n   0 ;  
 }  
 v o i d   F i l l A r r a y ( i n t   * _ m a s s ,   i n t   _ s i z e )  
 {  
 	 f o r   ( i n t   i   =   0 ;   i   <   _ s i z e ;   i + + )  
 	 	 _ m a s s [ i ]   =   r a n d ( )   %   1 5 0   +   3 0 ;  
 }  
 / / - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -     
 v o i d   P r i n t A r r a y ( i n t   * _ m a s s ,   i n t   _ s i z e )  
 {  
 	 f o r   ( i n t   i   =   0 ;   i   <   _ s i z e ;   i + + )  
 	 	 c o u t   < <   _ m a s s [ i ]   < <   "   " ;  
 	 c o u t   < <   e n d l ;  
 }  
 / / - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
 v o i d   F o r m a t T i m e F r o m S e c o n d s T o M i n u t e s ( i n t   _ T i m e ,   i n t   &   _ m i n u t e s ,   i n t   &   _ s e c o n d s )  
 {  
 	 _ m i n u t e s   =   _ T i m e   /   6 0 ;  
 	 _ s e c o n d s   =   _ T i m e   %   6 0 ;  
 }  
 / / - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
 v o i d   S h o w F o r m a t t e d T i m e ( i n t   _ I D ,   i n t   _ m i n u t e s ,   i n t   _ s e c o n d s )  
 {  
 	 i f   ( _ s e c o n d s   > =   1 0 )  
 	 {  
 	 	 c o u t   < <   " S p o r t s m a n   "   < <   _ I D   +   1   < <   " :   0 "   < <   _ m i n u t e s   < <   " : "   < <   _ s e c o n d s   < <   e n d l ;  
 	 }  
 	 e l s e  
 	 {  
 	 	 c o u t   < <   " S p o r t s m a n   "   < <   _ I D   +   1   < <   " :   0 "   < <   _ m i n u t e s   < <   " : 0 "   < <   _ s e c o n d s   < <   e n d l ;  
 	 }  
 }  
 / / - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
 i n t   P r o m t F o r I n p u n t ( )  
 {  
 	 i n t   m i n u t e s ,   s e c o n d s ;  
 	 b o o l   f l a g   =   f a l s e ;  
 	 d o  
 	 {  
 	 	 c o u t   < <   " \ n E n t e r   t i m e   i n   f o r m a t   M M : C C \ n ( R e m e m b e r !   Y o u   c a n   e n t e r   t i m e   o n l y   f r o m   0 0 : 0 0   t o   0 3 : 0 0 ) \ n m i n u t e s   =   " ;  
 	 	 c i n   > >   m i n u t e s ;  
 	 	 c o u t   < <   " s e c o n d s   =   " ;  
 	 	 c i n   > >   s e c o n d s ;  
 	 	 i f   ( ( m i n u t e s   <   3 )   & &   ( m i n u t e s   > =   0 )   & &   ( s e c o n d s   > =   0 )   & &   ( s e c o n d s   > =   1 0 )   & &   ( s e c o n d s   <   6 0 ) )  
 	 	 {  
 	 	 	 c o u t   < <   " \ n t i m e   =   0 "   < <   m i n u t e s   < <   " : "   < <   s e c o n d s ;  
 	 	 	 f l a g   =   t r u e ;  
 	 	 }  
 	 	 e l s e  
 	 	 {  
 	 	 	 i f   ( ( m i n u t e s   <   3 )   & &   ( m i n u t e s   > =   0 )   & &   ( s e c o n d s   > =   0 )   & &   ( s e c o n d s   <   1 0 ) )  
 	 	 	 {  
 	 	 	 	 c o u t   < <   " \ n t i m e   =   0 "   < <   m i n u t e s   < <   " : 0 "   < <   s e c o n d s ;  
 	 	 	 	 f l a g   =   t r u e ;  
 	 	 	 }  
 	 	 	 e l s e  
 	 	 	 {  
 	 	 	 	 i f   ( ( m i n u t e s   = =   3 )   & &   ( s e c o n d s   = =   0 ) )  
 	 	 	 	 {  
 	 	 	 	 	 c o u t   < <   " \ n t i m e   =   0 "   < <   m i n u t e s   < <   " : 0 "   < <   s e c o n d s ;  
 	 	 	 	 	 f l a g   =   t r u e ;  
 	 	 	 	 }  
 	 	 	 	 e l s e  
 	 	 	 	 {  
 	 	 	 	 	 c o u t   < <   " \ n W r o n g !   Y o u   n e e d   t o   e n t e r   t i m e   o n l y   f r o m   0 0 : 0 0   t o   0 3 : 0 0 \ n P l e a s e , t r y   a g a i n . \ n " ;  
 	 	 	 	 }  
 	 	 	 }  
 	 	 }  
 	 }   w h i l e   ( f l a g   ! =   t r u e ) ;  
 	 r e t u r n   m i n u t e s   *   6 0   +   s e c o n d s ;  
 }  
 / / - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
 v o i d   G e t C o u n t ( i n t   t i m e ,   i n t   * _ m a s s ,   i n t   _ s i z e ,   i n t &   c o u n t )  
 {  
 	 c o u n t   =   0 ;  
 	 f o r   ( i n t   i   =   0 ;   i   <   _ s i z e ;   i + + )  
 	 	 i f   ( t i m e   <   _ m a s s [ i ] )  
 	 	 	 c o u n t + + ;  
 } 